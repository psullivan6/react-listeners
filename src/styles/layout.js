let config = {
  gutter       : 30,
  gutterSmall  : 10,
  minTouchSize : 40,
  gridUnit     : 120,
  gridUnits    : {},
};

// Create both single entries for each grid unit value as well as an object of index key value pairs
for (let index = 0; index < 13; index += 1) {
  const value = (index === 0) ? 0 : (config.gridUnit * index) + ((index - 1) * config.gutterSmall);
  config[`gridUnit${index}`] = value;
  config.gridUnits[index] = value;
};

// Parse through the integer values and return 'px'-appended values
// [TODO] Probably a cleaner way to do this
Object.keys(config).map(key => {
  const value = config[key];

  if (typeof value === 'number') {
    config[`${key}Px`] = `${value}px`;
  }

  return false;
});

export default config;
