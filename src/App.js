// Utilities
import React, { Component, Fragment } from 'react';

// Relative Utilities
import './utilities/Listener';
import GlobalListeners from './utilities/GlobalListeners';

// Components
import Panel from './components/Panel.jsx';


class App extends Component {
  componentDidMount() {
    GlobalListeners.add();
  }

  componentWillUnmount() {
    GlobalListeners.remove();
  }

  render() {
    return (
      <Fragment>
        <Panel headline='Panel 1' />
        <Panel headline='Panel 2' />
        <Panel headline='Panel 3' />
        <Panel headline='Panel 4' />

        <button onClick={ () => GlobalListeners.remove('resize') }>REMOVE THE RESIZE</button>
        <button onClick={ () => GlobalListeners.remove('scroll') }>REMOVE THE SCROLL</button>

        <button onClick={ () => GlobalListeners.add('resize') }>ADD THE RESIZE</button>
        <button onClick={ () => GlobalListeners.add('scroll') }>ADD THE SCROLL</button>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi harum facere eveniet, dicta porro ipsum similique soluta delectus excepturi provident nam, pariatur molestias! Facere, maiores, cupiditate? Commodi, voluptas architecto ipsa.</p>
      </Fragment>
    );
  }
}

export default App;
