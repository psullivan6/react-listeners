// Libs / Helpers
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Relative Utilities
import withWindowDimensions from '../utilities/withWindowDimensions.jsx';

// Components
// [NOTE] INSERT COMPONENT IMPORT STATEMENTS HERE (if any)

// Services
// [NOTE] INSERT SERVICE IMPORT STATEMENTS HERE (if any)

// Variables
// [NOTE] INSERT const OR let STATEMENTS HERE (if any)

// Styles
const colors = ['#ff0033','#ff4233','#ff5f32','#ff7730','#ff8a2f','#ff9e2c','#ffaf2a','#ffbf26','#ffd021','#ffdf1b','#ffef11','#ffff00'];

class Panel extends Component {
  render() {
    const { headline, windowDimensions: { width, gridUnit } } = this.props;

    return (
      <section className="Panel" style={{ backgroundColor: colors[gridUnit - 1] }}>
        <h1>{ headline }</h1>
        <h2>{ `Window width: ${width}` }</h2>
      </section>
    );
  }
}

Panel.propTypes = {
  TEST: PropTypes.string
};

Panel.defaultProps = {
  TEST: 'TEST PROP DEFAULT VALUE'
};

export default withWindowDimensions(Panel);
