class Listener {
  constructor() {
    this.calls = {};
    this.events = {};
  }

  on(key, callback, useLastCall) {
    if (!this.events[key]) {
      this.events[key] = [];
    }

    this.events[key].push(callback);
    if (useLastCall && typeof this.calls[key] !== 'undefined') {
      callback(this.calls[key]);
    }
  }

  clear(key, callback = null) {
    // If a callback is passed and the events array has additional callbacks,
    // then just remove the argument callback
    if (callback != null && this.events[key].length > 1) {
      // Filter out the matching callback
      this.events[key] = this.events[key].filter((cb) => (
        cb !== callback
      ));
    } else {
      // remove the entire event array if there's no callback argument or if
      // that callback is the only item in the array
      delete this.events[key];
    }
  }

  trigger(key, data) {
    this.calls[key] = data || null;
    if (this.events[key]) {
      this.events[key].forEach(callback => callback(data));
    }
  }
}

export default window.Listener = window.Listener || new Listener();
