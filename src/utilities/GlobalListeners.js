// Libs / Helpers
import _debounce from 'lodash/debounce';

// Declare a debounced resize handler so the update isn't called a ton
const handleResize = _debounce((event) => {
  window.Listener.trigger('GLOBAL_RESIZE', event);
}, 100);

const handleScroll = (event) => (
  window.Listener.trigger('GLOBAL_SCROLL', event)
);

// Assign an object of event name -> handler functions to be looped below
const handlerMappings = {
  resize : handleResize,
  scroll : handleScroll,
};

// Declare an empty storage object for already added listeners
const addedListeners = {};


// [NOTE] This is scoped to a class to ensure the all function can dynamically call the `add` or
// `remove` function based on its argument
class GlobalListeners {
  // =============================================================================
  // Add + Remove
  // =============================================================================
  all(action) {
    Object.keys(handlerMappings).forEach(eventName => {
      this[action](eventName);
    });
  }

  // =============================================================================
  // Add
  // =============================================================================
  add(eventName) {
    // If an event name argument is passed, then just add and return the single event listener,
    // otherwise return all the event listeners declared above
    if (eventName != null) {

      if (addedListeners[eventName] != null) {
        // [TODO] Make this more precise language and/or error type
        throw new Error('Please do not declare multiple event listeners for the same event.');
      }

      // Store the listener so it can be checked above as to whether or not it was created yet
      addedListeners[eventName] = true;

      // Add the event listener and just return it as there's no return value required, so sending
      // the listeners default return value of `undefined` seems appropriate
      return window.addEventListener(eventName, handlerMappings[eventName]);
    }

    return this.all('add');
  }

  // =============================================================================
  // Remove
  // =============================================================================
  remove(eventName) {

    // If an event name argument is passed, then just add and return the single event listener,
    // otherwise return all the event listeners declared above
    if (eventName != null) {
      if (addedListeners[eventName] == null) {
        // [TODO] Make this more precise language and/or error type
        throw new Error(`The ${eventName} has already been removed, so no need to call this method.`);
      }

      // Remove the stored boolean for the listener since it's about to be removed
      delete addedListeners[eventName];

      return window.removeEventListener(eventName, handlerMappings[eventName]);
    }

    return this.all('remove');
  }
}

// [NOTE] This *should* only be imported once, so go ahead and export a new instantiated class
export default new GlobalListeners();
