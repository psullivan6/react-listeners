// Libs / Helpers
import React, { Component } from 'react';

// Styles
import Layout from '../styles/layout';

// Helper to get the name of of the WrappedComponent for easier debugging
const getDisplayName = WrappedComponent => WrappedComponent.displayName || WrappedComponent.name || 'Component';


// Adds a `windowDimensions` prop object to the argument component, which updates on window resize
export default function withWindowDimensions(WrappedComponent) {
  class WithWindowDimensions extends Component {
    constructor(props) {
      super(props);

      this.state = this.object();

      this.attachBindings();
    }

    componentDidMount() {
      window.Listener.on('GLOBAL_RESIZE', this.updateState);
    }

    componentWillUnmount() {
      window.Listener.clear('GLOBAL_RESIZE', this.updateState);
    }

    attachBindings() {
      this.updateState = this.updateState.bind(this);
    }

    // On resize, get the values again and re-set them
    // [TODO] Do this in a more performant way, only set values that have changed
    updateState() {
      this.setState(this.object());
    }

    object() {
      return {
        width       : this.width,
        height      : this.height,
        gridUnit    : this.gridUnit,
        orientation : this.orientation,
      };
    }

    get width() {
      return window.innerWidth;
    }

    get height() {
      return window.innerHeight;
    }

    get orientation() {
      return (this.width > this.height) ? 'landscape' : 'portrait';
    }

    get gridUnit() {
      const { gridUnits } = Layout;

      const matches = Object.values(gridUnits).filter(gridUnit => gridUnit < this.width);

      // Return the index of the matched grid unit
      return matches.length - 1;
    }

    render() {
      return <WrappedComponent windowDimensions={ { ...this.state } } { ...this.props } />;
    }
  }

  WithWindowDimensions.displayName = `WithWindowDimensions(${getDisplayName(WrappedComponent)})`;

  return WithWindowDimensions;
}
